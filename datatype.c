#include<stdio.h>

// int a = 10, b = 5;  // initialize ,  local vairable

int main () {
    // int a = 20, b = 40;  // integar variable 
    // printf("%d, %d",a,b);
    // printf("\n");

    // float fl = 4.5666;   //float variable
    // printf ("%f", fl);
    // printf("\t");
    // printf ("%0.2f", fl);
    // printf("\n");
    
    // char ch = 'h';      // will be only single value
    // printf("%c", ch);
    // return 0;

    // int l = 1000000000;
    // long long int ll = 10000000000000000; //for the long data type
    // printf("%d", l);
    // printf("\n");
    // printf("%lld", ll);

    float f = 3459.34209384032;
    double d = 3309.3530498503498509348;

    printf("%0.9f", f);
    printf("\n");
    printf("%0.15lf", d);
}